package org.xian.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.xian.common.MyProtobufDecoder;
import org.xian.common.MyProtobufEncoder;
import org.xian.common.Packet;
import org.xian.common.proto.LoginRequest_100101;
import org.xian.common.proto.SayHelloRequest_100102;

import java.net.InetSocketAddress;

/**
 * @author xiaoxian
 */
public class ProtobufClient implements Runnable{

    static Channel channel = null;

    public static void main(String[] args) throws InterruptedException {
        ProtobufClient client = new ProtobufClient();
        new Thread(client).start();
        // 等连接建立好
        Thread.sleep(1000);
        if (channel != null) {
            LoginRequest_100101 login = LoginRequest_100101.newBuilder().setUsername(1234).setPassword("world").build();
            Packet packet = new Packet(100101, login.toByteArray());
            channel.writeAndFlush(packet);
            Thread.sleep(1000);
            SayHelloRequest_100102 hello = SayHelloRequest_100102.newBuilder().setHello("Hello Hello Hello").build();
            Packet packet1 = new Packet(100102, hello.toByteArray());
            channel.writeAndFlush(packet1);
            Thread.sleep(2000);
            channel.close();
        }

    }

    public static void setChannel(Channel c) {
        channel = c;
    }

    @Override
    public void run() {
        try {
            start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void start() throws InterruptedException {
        EventLoopGroup group = new NioEventLoopGroup();
        try {

            Bootstrap bootstrap = new Bootstrap();
            int port = 8080;
            String host = "127.0.0.1";

            bootstrap.group(group).channel(NioSocketChannel.class)
                    // 远程服务器路径和端口号
                    .remoteAddress(new InetSocketAddress(host, port))
                    // Channel 通道的绑定 ChannelPipeline
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            // Protobuf 解码器
                            ch.pipeline().addLast(new MyProtobufDecoder(1024));
                            // Protobuf 编码器
                            ch.pipeline().addLast(new MyProtobufEncoder(1024));

                            // 绑定 PingClientHandler
                            ch.pipeline().addLast(new ProtobufClientHandler());
                        }
                    });
            // 链接到服务端和使用 ChannelFuture 接收返回的数据
            ChannelFuture future = bootstrap.connect().sync();
            future.channel().closeFuture().sync();

        } finally {
            group.shutdownGracefully().sync();
        }
    }
}
