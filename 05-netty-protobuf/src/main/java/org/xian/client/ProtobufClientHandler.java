package org.xian.client;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xian.common.Packet;
import org.xian.common.proto.LoginRequest_100101;


/**
 * @author xiaoxian
 */
@ChannelHandler.Sharable
public class ProtobufClientHandler extends SimpleChannelInboundHandler {

    Logger logger = LoggerFactory.getLogger(ProtobufClientHandler.class);

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        ProtobufClient.setChannel(ctx.channel());
    }


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object o) throws Exception {
        logger.info("ProtobufClientHandler Receiver -->  {}", o);
        // 返回的数据参考 DispatchServerHandler, 进行处理即可
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        ctx.close();
    }
}
