package org.xian.server;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xian.common.Packet;

/**
 * @author xian
 */
@ChannelHandler.Sharable
public class ProtobufServerHandler extends SimpleChannelInboundHandler {

    Logger logger = LoggerFactory.getLogger(ProtobufServerHandler.class);


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object o) throws Exception {
        Packet request = (Packet) o;
        logger.info("ProtobufServer Receiver From --> {}", ctx.channel().remoteAddress());
        DispatchRequestHandler.handlerRequest(ctx, request);
    }
}
