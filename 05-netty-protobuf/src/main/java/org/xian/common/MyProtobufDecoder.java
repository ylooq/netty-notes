package org.xian.common;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @author xian
 */
public class MyProtobufDecoder extends ByteToMessageDecoder {

    /**
     * 一个请求、响应的最大长度
     */
    private final int limit;

    public MyProtobufDecoder(int limit) {
        this.limit = limit;
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        // 请求或者响应包的长度 2 字节， 指令码的长度 4 字节
        if (in.readableBytes() < 6) {
            return;
        }
        // 这个请求或者响应的长度
        short length = in.readShort();
        if (length <= 0 || length > this.limit) {
            throw new IllegalArgumentException();
        }

        // 如果剩余可读的字节长度小于请求内容的长度
        if (in.readableBytes() < length) {
            // 重置读指针
            in.resetReaderIndex();
            return;
        }

        // 指令码
        int cmd = in.readInt();
        // 请求或者响应的内容
        byte[] payload = new byte[length - 4];
        in.readBytes(payload);
        out.add(new Packet(cmd, payload));

    }
}
