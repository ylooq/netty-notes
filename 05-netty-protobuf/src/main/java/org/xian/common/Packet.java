package org.xian.common;

/**
 * @author xian
 */
public class Packet {

    /** 请求的指令码 */
    private final int cmd;
    /** 请求的内容: 也就是 Protobuf 编码的二进制数据 */
    private final byte[] payload;

    public Packet(int cmd, byte[] payload) {
        this.cmd = cmd;
        this.payload = payload;
    }

    public int getCmd() {
        return cmd;
    }

    public byte[] getPayload() {
        return payload;
    }
}
