package org.xian.common;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ClassUtils {

    /**
     * 递归读取某一文件下的所有 class 文件
     *
     * @param dir         文件目录
     * @param packageName 文件包名
     * @return 所有的 class 文件
     * @throws ClassNotFoundException
     */
    public static List<Class<?>> getClasses(File dir, String packageName) throws ClassNotFoundException {
        ArrayList<Class<?>> classes = new ArrayList<>();
        for (File f : dir.listFiles()) {
            String name = f.getName();
            if (f.isDirectory()) {
                classes.addAll(getClasses(f, packageName + "." + name));
            }
            if (name.endsWith(".class")) {
                classes.add(Class.forName(packageName + "." + name.substring(0, name.length() - 6)));
            }
        }
        return classes;
    }

    /**
     * 读取某个 Java 包下的所有 class
     *
     * @param packageName 包名
     * @return 所有的 class 文件
     * @throws ClassNotFoundException
     */
    public static List<Class<?>> getClasses(String packageName) throws ClassNotFoundException {
        String path = packageName.replace('.', '/');
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        URL url = classloader.getResource(path);
        return getClasses(new File(url.getFile()), packageName);
    }
}
