package org.xian.server;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xian.protobuf.Responses;

/**
 * @author xian
 */
@ChannelHandler.Sharable
public class OneofProtobufServerHandler extends SimpleChannelInboundHandler {

    Logger logger = LoggerFactory.getLogger(OneofProtobufServerHandler.class);


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object o) throws Exception {
        Responses.LoginResp ok = Responses.LoginResp.newBuilder().setMessage("Ok").setToken("1234567890").build();
        Responses.Response.Builder resp = Responses.Response.newBuilder().setCmd(1000).setLogin(ok);
        ctx.writeAndFlush(resp);
    }
}
