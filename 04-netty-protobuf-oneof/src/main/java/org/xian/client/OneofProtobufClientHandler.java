package org.xian.client;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xian.protobuf.Requests;
import org.xian.protobuf.Responses;


/**
 * @author xiaoxian
 */
@ChannelHandler.Sharable
public class OneofProtobufClientHandler extends SimpleChannelInboundHandler {

    Logger logger = LoggerFactory.getLogger(OneofProtobufClientHandler.class);

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        Requests.LoginReq login = Requests.LoginReq.newBuilder().setUsername(123).setPassword("Netty").build();
        Requests.Request.Builder request = Requests.Request.newBuilder().setCmd(1000).setLogin(login);
        ctx.writeAndFlush(request);
    }


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object o) throws Exception {
        Responses.Response response = (Responses.Response) o;
        logger.info("OneofProtobufClientHandler Receiver -->  \n{}", response);
        ctx.close();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        ctx.close();
    }
}
