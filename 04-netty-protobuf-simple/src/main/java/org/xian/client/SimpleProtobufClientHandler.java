package org.xian.client;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xian.protobuf.user.UserRequest;
import org.xian.protobuf.user.UserResponse;


/**
 * @author xiaoxian
 */
@ChannelHandler.Sharable
public class SimpleProtobufClientHandler extends SimpleChannelInboundHandler {

    Logger logger = LoggerFactory.getLogger(SimpleProtobufClientHandler.class);

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        UserRequest request = UserRequest.newBuilder().setId(123).setName("Netty").setSex(0).build();
        ctx.writeAndFlush(request);
    }


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object o) throws Exception {
        UserResponse response = (UserResponse) o;

        logger.info("SimpleProtobufClient Receiver -->  {}", response);
        ctx.close();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        ctx.close();
    }
}
