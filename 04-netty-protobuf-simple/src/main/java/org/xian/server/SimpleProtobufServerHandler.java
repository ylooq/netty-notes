package org.xian.server;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xian.protobuf.user.UserRequest;
import org.xian.protobuf.user.UserResponse;

/**
 * @author xian
 */
@ChannelHandler.Sharable
public class SimpleProtobufServerHandler extends SimpleChannelInboundHandler {

    Logger logger = LoggerFactory.getLogger(SimpleProtobufServerHandler.class);


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object o) throws Exception {
        UserRequest request = (UserRequest) o;
        logger.info("SimpleProtobufServer Receiver --> \n {}", request);
        UserResponse response = UserResponse.newBuilder().setMessage("SimpleProtobufServer Send to " + request.getName()).build();
        ctx.writeAndFlush(response);
    }
}
