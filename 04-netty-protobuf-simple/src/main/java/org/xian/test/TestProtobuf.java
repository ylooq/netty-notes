package org.xian.test;

import com.google.protobuf.InvalidProtocolBufferException;
import org.xian.protobuf.user.UserRequest;

public class TestProtobuf {
    static byte[] encode(UserRequest request){
        return request.toByteArray();
    }

    static UserRequest decode(byte[] body) throws InvalidProtocolBufferException {
        return UserRequest.parseFrom(body);
    }

    public static void main(String[] args) throws InvalidProtocolBufferException {
        UserRequest request = UserRequest.newBuilder().setId(123).setName("李四").build();
        byte[] encode = encode(request);
        UserRequest request1 = decode(encode);
        System.out.println(request1.getName());
        System.out.println(request1.getId());
    }
}
