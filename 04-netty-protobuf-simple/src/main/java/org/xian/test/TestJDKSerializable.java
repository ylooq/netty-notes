package org.xian.test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;

public class TestJDKSerializable {
    public static void main(String[] args) throws IOException {
        UserInfo zhangsan = new UserInfo("zhangsan", 123);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(bos);
        os.writeObject(zhangsan);
        os.flush();
        os.close();
        System.out.println("JDK Serial size is " + bos.toByteArray().length);
        bos.close();
        System.out.println("The Byte Array Serializable size is " + zhangsan.codeC(ByteBuffer.allocate(1024)).length);

        int loop = 10000000;
        long startTimes = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            bos = new ByteArrayOutputStream();
            os = new ObjectOutputStream(bos);
            os.writeObject(zhangsan);
            os.flush();
            os.close();
            bos.toByteArray();
            bos.close();
        }
        long endTimes = System.currentTimeMillis();
        System.out.println("JDK Times " + (endTimes - startTimes) + "ms");


        startTimes = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            zhangsan.codeC(buffer);
        }
        endTimes = System.currentTimeMillis();
        System.out.println("Bytes Times " + (endTimes - startTimes)+ "ms");

    }
}
