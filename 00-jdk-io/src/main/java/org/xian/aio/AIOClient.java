package org.xian.aio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

/**
 * @author : xian
 */
public class AIOClient {
    private int port = 8080;
    private String host = "127.0.0.1";
    private AsynchronousSocketChannel asyncSocketChannel;

    public AIOClient() {
        try {
            this.asyncSocketChannel = AsynchronousSocketChannel.open();
            this.asyncSocketChannel.setOption(StandardSocketOptions.SO_KEEPALIVE, true);
            this.asyncSocketChannel.setOption(StandardSocketOptions.TCP_NODELAY, true);
            this.asyncSocketChannel.connect(new InetSocketAddress(this.host, this.port));
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            // 处理服务端返回消息的回调
            this.asyncSocketChannel.read(buffer, buffer, new CompletionHandler<>() {
                @Override
                public void completed(Integer result, ByteBuffer attachment) {
                    // 读取服务端户端的消息
                    attachment.flip();
                    byte[] bytes = new byte[attachment.remaining()];
                    attachment.get(bytes);
                    String message = new String(bytes, StandardCharsets.UTF_8);
                    System.out.println("Aio Client Received --> " + message);
                    attachment.clear();
                    if ("bye".equals(message)) {
                        try {
                            asyncSocketChannel.shutdownInput();
                            asyncSocketChannel.shutdownOutput();
                            asyncSocketChannel.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }else{
                        asyncSocketChannel.read(attachment, attachment, this);
                    }

                }

                @Override
                public void failed(Throwable exc, ByteBuffer attachment) {
                    exc.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message){
        try {
            this.asyncSocketChannel.write(ByteBuffer.wrap(message.getBytes(StandardCharsets.UTF_8))).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        AIOClient aioClient = new AIOClient();
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String input = scanner.nextLine();
            aioClient.sendMessage(input);
            if ("bye".equals(input)) {
                break;
            }
        }
    }
}
