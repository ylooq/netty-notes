package org.xian.reactor;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Basic Reactor Design
 * 单线程的 Reactor：等同于 org.xian.nio.NIOServer
 */
public class MultipleReactor implements Runnable {

    public static ExecutorService pool = Executors.newFixedThreadPool(5);

    final Selector mainSelector;

    final ServerSocketChannel serverSocket;

    public MultipleReactor(int port) throws IOException {
        mainSelector = Selector.open();
        serverSocket = ServerSocketChannel.open();
        serverSocket.socket().bind(new InetSocketAddress(port));
        serverSocket.configureBlocking(false);
        SelectionKey selectionKey = serverSocket.register(mainSelector, SelectionKey.OP_ACCEPT);
        selectionKey.attach(new Acceptor());
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                mainSelector.select();
                Set<SelectionKey> selectionKeys = mainSelector.selectedKeys();
                Iterator<SelectionKey> keys = selectionKeys.iterator();
                while (keys.hasNext()) {
                    dispatch(keys.next());
                }
                selectionKeys.clear();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void dispatch(SelectionKey key) {
        Runnable r = (Runnable) key.attachment();
        if (r != null) {
            r.run();
        }
    }

    class Acceptor implements Runnable {
        // SubReactor 里使用的 Selector
        Selector[] subSelectors;

        int next = 0;

        public Acceptor() {
            subSelectors = new Selector[3];
            // sub Reactor 的初始化，这里使用三个 subReactor
            for (int i = 0; i < subSelectors.length; i++) {
                try {
                    subSelectors[i] = Selector.open();
                    new Thread(new SubReactor(subSelectors[i])).start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void run() {
            try {
                SocketChannel s = serverSocket.accept();
                if (s != null) {
                    // 新的客户端轮训交给 subReactor 处理
                    new Handler(subSelectors[next], s);
                }
                if (++next == subSelectors.length) next = 0;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class SubReactor implements Runnable {

        Selector subSelector;

        public SubReactor(Selector subSelector) {
            this.subSelector = subSelector;
        }

        @Override
        public void run() {
            while (!Thread.interrupted()) {
                try {
                    subSelector.select();
                    Set<SelectionKey> selectionKeys = subSelector.selectedKeys();
                    Iterator<SelectionKey> keys = selectionKeys.iterator();
                    while (keys.hasNext()) {
                        subDispatch(keys.next());
                    }
                    selectionKeys.clear();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public void subDispatch(SelectionKey key) {
            Runnable r = (Runnable) key.attachment();
            if (r != null) {
                r.run();
            }
        }
    }

    class Handler implements Runnable {
        final SocketChannel socket;
        final SelectionKey sk;

        final Selector selector;

        ByteBuffer input = ByteBuffer.allocate(1024);
        ByteBuffer output = ByteBuffer.allocate(1024);

        final int READING = 0, SENDING = 1, PROCESSING = 3;

        int stage = READING;


        public Handler(Selector sel, SocketChannel s) throws IOException {
            socket = s;
            socket.configureBlocking(false);
            sk = socket.register(sel, 0);
            sk.attach(this);
            sk.interestOps(SelectionKey.OP_READ);
            sel.wakeup();
            selector = sel;
        }

        @Override
        public void run() {
            try {
                if (stage == READING) read();
                if (stage == SENDING) send();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        synchronized void read() throws IOException {
            socket.read(input);
            if (inputIsComplete()) {
                stage = PROCESSING;
                MultipleReactor.pool.execute(new Processer());
            }
        }

        synchronized void send() throws IOException {
            output.flip();
            socket.write(output);
            if (outputIsComplete()) {
                output.clear();
                stage = READING;
                sk.interestOps(SelectionKey.OP_READ);
            }
        }

        void process() {
            input.flip();
            byte[] bytes = new byte[input.remaining()];
            input.get(bytes);
            String message = new String(bytes, StandardCharsets.UTF_8);
            System.out.println("Server Received:" + message);
            String request = "Server Re:" + message;
            input.compact();
            output.put(request.getBytes(StandardCharsets.UTF_8));
        }

        synchronized void processAndHandOff() {
            process();
            stage = SENDING;
            sk.interestOps(SelectionKey.OP_WRITE);
            selector.wakeup();
        }

        boolean inputIsComplete() {
            return input.hasRemaining();
        }

        boolean outputIsComplete() {
            return !output.hasRemaining();
        }

        class Processer implements Runnable {

            @Override
            public void run() {
                processAndHandOff();
            }
        }
    }

    public static void main(String[] args) throws IOException {
        MultipleReactor reactor = new MultipleReactor(8080);
        new Thread(reactor).start();
    }
}
