package org.xian.udp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.charset.StandardCharsets;

/**
 * @author : xian
 */
public class UdpSender {
    public static void main(String[] args) throws IOException {
        DatagramChannel channel = DatagramChannel.open();

        channel.configureBlocking(false);
        channel.connect(new InetSocketAddress("127.0.0.1", 9999));
        ByteBuffer buffer = ByteBuffer.wrap("Hello UDP".getBytes(StandardCharsets.UTF_8));
        channel.write(buffer);
        // 如果是接受数据，则参考 UdpReceiver
    }
}
