package org.xian.buffer;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * @author : xian
 */
public class DoubleFlipTest {
    public static void main(String[] args) {
        // 纯粹好奇调用两次 flip 后会发生什么情况，结果果然是这样
        // flip : limit 等于原来的 position， 然后 position 归零，
        ByteBuffer buffer = ByteBuffer.allocate(128);
        buffer.put("Hello".getBytes(StandardCharsets.UTF_8));
        System.out.println("no flip : limit ->" + buffer.limit() + ", position ->" + buffer.position());
        buffer.flip();
        System.out.println("flip 1 : limit ->" + buffer.limit() + ", position ->" + buffer.position());
        buffer.flip();
        System.out.println("flip 2 : limit ->" + buffer.limit() + ", position ->" + buffer.position());
    }
}
