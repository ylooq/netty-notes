package org.xian.bio;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class BIOServer {
    public static void main(String[] args) {
        int port = 8080;
        ServerSocket serverSocket = null;
        try {
            // 1、服务端启动并监听某个端口号
            serverSocket = new ServerSocket(port);
            while (true){
                // 2、阻塞等待客户端连接
                // 3、连接建立后交给线程进行处理
                Socket socket = serverSocket.accept();
                // 如果不启动一个线程处理，则必须等上一个连接断开，才可以接受下一个连接
                // new Thread(new BIOServerHandler(socket)).run()
                // 缺点非常明显：一个长链接就需要一个单独的线程进行处理；使用线程池则又需要频繁地建立 TCP 连接
                new Thread(new BIOServerHandler(socket)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(serverSocket != null){
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }
}
